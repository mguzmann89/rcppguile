#' RcppGuile: A package for calling scheme from R.
#'
#' The RcppGuile provides a main function `call_guile()`.
#' 
#' @section RcppGuile functions:
#' 
#' call_guile
#'
#' @docType package
#' @import Rcpp
#' @importFrom Rcpp evalCpp
#' @name RcppGuile
#' @useDynLib RcppGuile, .registration=TRUE
NULL
#> NULL
