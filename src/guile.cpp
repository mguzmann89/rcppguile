#include <Rcpp.h>
#include <cstddef>
#include <stdio.h>
#include <libguile.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <fstream>

using namespace Rcpp;

/*
 Convert and RObject to a corresponding SCM type.
 Implemented so far:

 * List
 * IntegerVector
 * NumericVector
 * LogicalVector
 * StringVector

 If a vector has lenght 1 and no names it is assumed to be a scalar
 
*/
SCM obj_to_scm(RObject obj) {
  if (is<List> (obj)) { // if it is a list, build the list recursively
    List obj_l = as<List>(obj); // otherwise it doesn't work
    size_t len = obj_l.size();

    if (len == 0) // if list is empty
      return (SCM_EOL);
    else {
      // we build the list backwards to get it in the right order
      SCM list_res = scm_list_1(obj_to_scm(obj_l[0]));

      for (size_t i = 1; i < len; i++) {
	list_res = scm_cons(obj_to_scm(obj_l[i]), list_res);
      }
      
      return scm_reverse(list_res);
    }
  } else {
    size_t size_o = Rf_xlength(obj);
    bool is_scalar = !obj.hasAttribute("names");
    if (is_scalar & (size_o == 1)) { // if we want to make a scalar
      if (is<IntegerVector> (obj))
	return scm_from_long_long(as<long long> (obj));
      else if (is<LogicalVector> (obj))
	return scm_from_bool(as<bool> (obj));
      else if (is<NumericVector> (obj))
	return scm_from_double(as<double> (obj));
      else if (is<StringVector> (obj)) {
	std::string tmp_str = as<std::string> (obj);
	return scm_from_locale_string(tmp_str.c_str());
      } else
	stop("Cannot convert value to scheme");
    } else { // if we want to make a vector
      SCM vec_res;
      
      if (is<IntegerVector> (obj)) {
	IntegerVector obj =  as<IntegerVector> (obj);
	vec_res = scm_c_make_vector(size_o, scm_from_int(0));
	for (size_t i = 0; i < size_o; i++)
	  scm_c_vector_set_x(vec_res, i, scm_from_int(obj[i]));
	return vec_res;
      } else if (is<LogicalVector> (obj)) {
	vec_res = scm_c_make_vector(size_o, scm_from_bool(true));
	LogicalVector obj =  as<LogicalVector> (obj);
	for (size_t i = 0; i < size_o; i++)
	  scm_c_vector_set_x(vec_res, i, scm_from_bool(obj[i]));
	return vec_res;
      } else if (is<NumericVector> (obj)) {
	vec_res = scm_c_make_vector(size_o, scm_from_double(0));
	IntegerVector obj = as<IntegerVector> (obj);
	for (size_t i = 0; i < size_o; i++)
	  scm_c_vector_set_x(vec_res, i, scm_from_double(obj[i]));
	return vec_res;
      } else if (is<StringVector> (obj)) {
	vec_res = scm_c_make_vector(size_o, scm_from_locale_string(""));
	StringVector obj = as<StringVector> (obj);
	std::vector<std::string> str_tmp (size_o);
	for (size_t i = 0; i < size_o; i++)
	  str_tmp[i] = obj[i];
	for (size_t i = 0; i < size_o; i++) {
	  scm_c_vector_set_x(vec_res, i, scm_from_locale_string(str_tmp[i].c_str()));
	}
	return vec_res;
      }
    
      else {
	stop("Cannot convert value to scheme");
      }
    }
  }
}

/*--------------------------------------------------------------------------------
  Check whether a list is an alist
  --------------------------------------------------------------------------------
*/
bool scm_is_alist(SCM obj_scm) {
  if (scm_is_true(scm_list_p (obj_scm))) {
    size_t len = scm_to_size_t(scm_length(obj_scm));
    bool isa = true;
    for (size_t i = 0; i < len; i++ ) {
      SCM car_obj = scm_car (obj_scm);
      if (scm_is_true(scm_list_p (car_obj)) | !scm_is_pair(car_obj))
	isa = false;
      obj_scm = scm_cdr (obj_scm);
    }
    return isa;
  } else
    return false;
}

/*
  Cast alist to named list.
  We cannot return a named vector because: '((a . 12) (b . "hola"))
  
*/
List scm_alist_to_obj(SCM& obj_scm);

/*
  Convert SCM return values into Rvalues.
  Supported for now:
  * integers : 1 23 10e10
  * floats   : 0.1 1/7
  * booleans : #f #t
  * Strings  : "string"
  * Symbols  : string -> "string" [no way to return them otherwise]
  * lists    : (1 2 3 4)
  * arrays  : #(1 2 3) -> (1 2 3) [we can't know what types will be in there]

  TODO:

  * alists      -> named list   (?)
  * hash tables -> named vector (?)
*/ 
RObject scm_to_obj(SCM& obj_scm) {
  if (scm_is_true(scm_list_p(obj_scm)) &
      !scm_is_alist(obj_scm)) {                                      // proper list
    size_t len = scm_to_size_t(scm_length(obj_scm));
    List res(len);
    for (size_t i = 0; i < len; i++) {
      SCM car_obj = scm_car (obj_scm);
      res[i] = scm_to_obj(car_obj);
      obj_scm = scm_cdr (obj_scm);
    }
    return res;
  } else if (scm_is_alist(obj_scm)) {                               // alist
    List res = scm_alist_to_obj(obj_scm);
    return res;
  } else if (scm_is_true(scm_hash_table_p(obj_scm))) {              // hash-table
    const SCM cons_func = scm_variable_ref(scm_c_lookup("cons"));
    SCM alist = scm_hash_map_to_list(cons_func, obj_scm);
    List res = scm_alist_to_obj(alist);
    return res;
  } else if (scm_is_exact_integer(obj_scm)) {                       // exact int
    IntegerVector res(1);
    res[0] = scm_to_long_long(obj_scm);
    return res;
  } else if (scm_is_bool(obj_scm)) {                                // bool
    LogicalVector res(1);
    res[0] = scm_to_bool(obj_scm);
    return res;
  } else if (scm_is_real(obj_scm) | scm_is_rational(obj_scm)) {     // double
    NumericVector res(1);
    res[0] = scm_to_double(obj_scm);
    return res;
  } else if (scm_is_string(obj_scm)) {                              // string
    StringVector res(1);
    res[0] = std::string(scm_to_locale_string(obj_scm));
    return res;
  } else if (scm_is_symbol(obj_scm)) {                              // symbol
    StringVector res(1);
    res[0] = std::string(scm_to_locale_string(scm_symbol_to_string(obj_scm)));
    return res;
  } else if (scm_is_array (obj_scm)) {                              // array (->list)
    SCM obj_list = scm_array_to_list(obj_scm);
    return scm_to_obj(obj_list);
  } else 
    stop("Cannot convert scheme return value to a valid R value.");
}

/*
 Convert an alist into a named list.
 */
List scm_alist_to_obj(SCM& obj_scm) {
  size_t len = scm_to_size_t(scm_length(obj_scm));
  StringVector names(len);
  List values(len);
  for (size_t i = 0; i < len; i++ ) {
    SCM car_obj = scm_car (obj_scm);
    SCM ccar = scm_car(car_obj); // name
    SCM ccdr = scm_cdr(car_obj); // value
    values[i] = scm_to_obj(ccdr);
    std::string name_i;
    if (scm_is_string(ccar)) {
       name_i = scm_to_locale_string(ccar);
    } else if (scm_is_number(ccar)) {
      name_i = scm_to_locale_string(scm_number_to_string(ccar, scm_from_int(10)));
    } else if (scm_is_symbol(ccar)) {
      name_i = scm_to_locale_string(scm_symbol_to_string(ccar));
    } else {
      name_i = "NA";
    }
    names[i] = name_i;
    
    // go to nect
    obj_scm = scm_cdr(obj_scm);
  }
  values.attr("names") = names;
  return values;
}


// simple helper to check if file exists
bool file_exists_cpp (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

//' call_guile
//' 
//' Call a guile function defined in `file`
//'
//' @param file Path to file
//' @param fun_name Name of function
//' @param args List of arguments to be passed to fun_name
//' 
//' @return A string 
//'
//' @export
//' 
//' @examples
//'
//' call_guile("test.scm", "factorial", list(10))
//' 
// [[Rcpp::export]]
RObject call_guile(std::string file,
		   std::string fun_name,
		   List args) {

  // check file exists
  if (!file_exists_cpp(file))
    stop("File: " + file + " does not exist");

  // start guile
  scm_init_guile();

  scm_c_primitive_load(file.c_str());

  /* To avoid crashing, we look for the function using scm_module_variable.
   If the function is not found, it returns #f instead of an error.
   **TODO: figure out how to handle errors raised by guile.
  */
  SCM current_module = scm_current_module();
  SCM func_var = scm_module_variable(current_module,
				     scm_from_locale_symbol(fun_name.c_str()));

  // if not function nout found, stop and exit.
  if (scm_to_bool(scm_boolean_p(func_var)))
    stop("Function: " + fun_name + " not found in file: " + file + " .\n");

  SCM func = scm_variable_ref(func_var);
    
  // SCM func = scm_variable_ref(scm_c_lookup(fun_name.c_str()));

  // Build args for scheme
  SCM args_conv[args.size()];

  // the pre-checks are needed, otherwise it crashes.
  for (size_t i = 0; i < args.size(); i++) {
    if (is<List> (args[i])) {
      args_conv[i] = obj_to_scm(as<List> (args[i]));
    } else if (is<LogicalVector> (args[i])) {
      args_conv[i] = obj_to_scm(as<LogicalVector> (args[i]));
    } else if (is<StringVector> (args[i])) {
      args_conv[i] = obj_to_scm(as<StringVector> (args[i]));
    } else if (is<IntegerVector> (args[i])) {
      args_conv[i] = obj_to_scm(as<IntegerVector> (args[i]));
    } else if (is<NumericVector> (args[i])) {
      args_conv[i] = obj_to_scm(as<NumericVector> (args[i]));
    } else {
      stop("Type of argument not yet supported.");
    }
  }

  // return object.
  SCM res_scm;
    
  // Check that the number of required arguments matches
  // build module.
  SCM module_name = scm_list_3(scm_from_locale_symbol("system"),
			  scm_from_locale_symbol("vm"),
			  scm_from_locale_symbol("program"));
  //  load module
  SCM module = scm_resolve_module(module_name);
  //  find `program-arguments-alist`
  SCM nargs_func = scm_module_variable(module,
				       scm_from_locale_symbol("program-arguments-alist"));
  SCM nargs_res = scm_call_1(scm_variable_ref(nargs_func), func);
  // the number of required arguments is given by the length of the car - 1
  int nargs = scm_to_int(scm_length(scm_car(nargs_res))) - 1;

  // sto if the wrong number of arguments is passed
  if (nargs != args.size())
    stop("Wrong number of arguments passed to scheme function. Function takes: " +
	  std::to_string(nargs) +
	  " but " +
	  std::to_string(args.size()) +
	  " arguments passed.");
  
  res_scm = scm_call_n(func, args_conv, args.size());

  // build return object
  RObject result = scm_to_obj(res_scm);;
  
  return result;
}
