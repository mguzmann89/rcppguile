# RcppGuile

Interface to connect R with Guile.

This work is based on an [answer](https://stackoverflow.com/questions/65340724/linking-guile-to-rcpp/65345260?noredirect=1#comment115534546_65345260) given by Dirk Eddelbuettel.

This is still *very* WIP, please test and open issues if you encounter any errors. Expect crashes.

## Installation

You first have to install Guile 3 or higher `(sudo apt install guile-3.0-dev)` and then you can just run `devtools::install_gitlab("mguzmann89/rcppguile")` and it should work.

## Examples

To get to it:

Assuming you have the file `my-funs.scm`

```scheme
(define (sum-fun n) 
  (if (zero? n) 0 (+ n (sum-fun (- n 1)))))
```

You can call `call_guile("my-funs.scm", "sum-fun", list(10))`.

## Why thou?

### What's wrong with R?

Recursion. R is not great for recursive functions and can be very slow.

Imagine we define a recursive sum function in R as:

```R
my_sum <- function(x) {
    if (x == 0) return (0)
    else       return (x + my_sum(x-1))
}
```

And compare it to the scheme definition above (including overhead from data transfer and all):

```R
microbenchmark::microbenchmark(RcppGuile::call_guile("./scheme/test-funs.scm", "sum-fun", 1000)
                             , my_sum(1000))

                       expr      min       lq      mean   median       uq      max neval
 RcppGuile::call_guile(...) 1541.856 1611.012 1891.4380 1650.671 1724.595 5588.984   100
                my_sum(100)   39.544   43.248   55.9719   47.838   62.078  151.083   100


microbenchmark::microbenchmark(RcppGuile::call_guile("./scheme/test-funs.scm", "sum-fun", 1000)
                             , my_sum(1000))

                       expr      min       lq      mean   median        uq      max neval
 RcppGuile::call_guile(...) 1554.484 1744.619 2061.5989 1826.688 2015.1695 4846.310   100
               my_sum(1000)  448.123  499.300  651.0811  567.273  662.3625 2001.653   100
```

So we see there is some real overhead from starting guile and passing the data. However, adding 900 additional steps increases R's time by 600 while it increases guile's by 100 microseconds.
Additionally, guile can be called on any size inputs, R cannot do more than 1054 recursive calls (in my machine).

Where we do see a bit of a difference is if we try to grow a list. This is very slow in R:

```R
fun_strings <- function (j) {
    res <- vector(mode = "list", length = j)
    for (i in 1:j) {
        res[i] <- "string"
    }
    res
}

fun_strings_rec <- function (j) {
    if (j == 0) return (list())
    else       return (c("strings", fun_strings_rec(j-1)))
}
```

vs.

```scheme
(define (fun-strings j)
  (if (zero? j) '()
      (cons "string" (fun-strings (- j 1)))))
```

The benchmarks are:

```R
                                min        lq       mean     median         uq       max neval
RcppGuile::call_guile(...) 2036.804  2303.252  2696.2270  2489.1030  2693.0980  6816.702   100
fun_strings                 198.389   226.417   343.4032   252.0995   289.7775  4808.184   100
fun_strings_rec            4532.545  5048.704  5951.8366  5330.6145  5587.7535 11656.144   100
```

Finally, I tried to increase the work done within guile itself and see whether it provided any advantages over R. For this we call sum-fun 1000 times within guile:

```scheme
(define (sum-fun-n j n)
  (if (zero? j) '()
      (cons (sum-fun n)
	    (sum-fun-n (- j 1) n))))
```

and for R:

```R
my_sum_n <- function(x, j) {
    sapply(1:j, function(j) {
        my_sum(x)
    })
}
```

The results are:

```R
                      expr      min       lq     mean   median       uq      max neval
RcppGuile::call_guile(...) 159.1032 164.8269 172.6143 166.4375 170.0080 237.6847   100
      my_sum_n(1000, 1000) 451.9607 490.4144 541.2769 502.4332 572.2509 918.3879   100
```

So, guile is at almost 3-4 times faster in the long-run. This is no replacement for c++ in terms of speed, but it is also not slower than writing R code.

If you have any tips for how to speed up things I'll be happy to hear.

### Why not Rcpp

I **hate** c++, if you look at the code you'll see how terrible my c++ is.
There is also the issue that c++ is rather cumbersome for functional programing.
Finally, for symbolic programming scheme is simply best of class, and I do symbolic programming from time to time.

### To sum up:

If you want to use scheme to extend R, now you (sort of) can!

## Supported data transfer

### From R

You can pass: 

* list -> list
* NumericVector -> vector
* IntegerVector -> vector
* StringVector -> vector
* LogicalVector -> vector
* String -> string

Unnamed vectors of length 1 are passed as scalars of the corresponding type.

There is no interface for either data.frames or matrix.

### From Scheme

You can pass:

* list -> list
* vector -> list
* alist -> named list
* hash-table -> named list
* String -> StringVector
* Boolean -> LogicalVector
* Number -> NumericVector
* Integer -> IntegerVector

## TODO:

### High priority

* [] support NA
* [] support typed vectors (for now you're stucked with scheme's generic vectors which support any datatype #(1 "h" 1.2)).
* [] support data.frames
* [] support matrix
* [] support other scheme return values (sets? ...)

### Low priority

* [] support S4 objects. 
* [] support (scheme) structs

### Comments

Supporting NA is tricky because Rcpp distinguishes between int_NA, double_NA, etc.

Supporting typed vectors, data.frames, and matrices will likely require writing a set of foreign objects.

Supporting S4->struct objects will be difficult and I don't think this is a high priority issue...
