(use-modules (ice-9 hash-table))

;; benchmarks

(define (fun-strings j)
  (if (zero? j) '()
      (cons "string" (fun-strings (- j 1)))))

;; return integers

(define (factorial n) 
  (if (zero? n) 1 (* n (factorial (- n 1)))))

(define (sum-fun n) 
  (if (zero? n) 1 (+ n (sum-fun (- n 1)))))

(define (sum-fun-n j n)
  (if (zero? j) '()
      (cons (sum-fun n)
	    (sum-fun-n (- j 1) n))))

(define mult-test-1
  (lambda (x y) (* x y)))

(define mult-test-2
  (lambda (x y z) (* x y z)))

;; return numbers

(define div-test-1
  (lambda (x y) (/ x y)))

(define div-test-2
  (lambda (x) (/ x 9.9)))

;; return strings

(define ret-str-1
  (lambda () "test"))

(define ret-str-2
  (lambda () (string #\x #\y #\z)))

(define ret-str-3
  (lambda (x)
    (display x)
    (display "\n")
    x ))

(define ret-str-4
  (lambda (x)
    (display (string? x))
    (display "\n")
    x ))

;; return lists

(define ret-list-test-1
  (lambda (x) (list x x)))

(define ret-list-test-2
  (lambda (x) (list x (list x (list x x)))))

(define ret-list-test-3
  (lambda (x) x))

;; return vectors

(define ret-vec-test-1
  (lambda () #(1 2 3 4)))

(define ret-vec-test-2
  (lambda () #2((1 2) (3 4))))

;; what about alists?

(define ret-alist-test-1
  (lambda () '(("one" . 1) ("two" . 2) ("tree" . 3) ("four" . 4))))

(define ret-alist-test-2
  (lambda () '(("test" . "pair"))))

(define ret-alist-test-3
  (lambda () '(("test" . "pair") (2 . "pair") (chantar . "pair"))))

;; and hash-tables

(define ret-hash-test-1
  (lambda () (alist->hash-table
	 '(("test" . "pair") (2 . "pair") (chantar . "pair")))))

;; pass vector

(define pass-vec-test-1
  (lambda (x)
    (let ((y (vector-ref x 0)))
      (display y)
      (display "\n")
      y)))

(define pass-vec-test-2
  (lambda (x) x))
